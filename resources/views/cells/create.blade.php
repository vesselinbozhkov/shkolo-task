@extends('layouts.app')

@section('content')
    <div class="form-wrapper">
        <form action="{{ route('cells.store') }}" method="POST">

            @include('cells.form')

            <div class="input-group">
                <button type="submit" class="submit">Create!</button>
            </div>
        </form>
    </div>
@endsection
