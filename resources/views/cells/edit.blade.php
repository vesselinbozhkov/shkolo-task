@extends('layouts.app')

@section('content')
    <div class="form-wrapper">
        <form action="{{ route('cells.update', ['cell' => $cell]) }}" method="POST" id="form-update">
            @method('PUT')

            @include('cells.form')

        </form>
        <form action="{{ route('cells.destroy', ['cell' => $cell]) }}" id="form-delete" method="POST">
            @csrf
            @method('DELETE')
        </form>
        <div class="input-group">
            <button type="submit" class="submit" form="form-update">Update!</button>
            <button type="submit" class="submit delete" form="form-delete"
                    onclick="confirm('Are you sure you want to delete it?')">
                Delete!
            </button>
        </div>
    </div>
@endsection
