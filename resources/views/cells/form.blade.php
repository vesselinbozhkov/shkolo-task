@csrf
<div class="input-group">
    @if(isset($position))
        <input type="hidden" value="{{ $position }}" name="position">
        @error('position')
        <div class="danger">{{ $message }}</div>
        @enderror
    @endif
</div>
<div class="input-group">
    <label for="title">Title:</label>
    <input id="title" type="text" name="title" value="{{ old('title', $cell->title ?? '') }}" required
           placeholder="Sample Title"
           class="@error('title') invalid @enderror">
    @error('title')
        <div class="danger">{{ $message }}</div>
    @enderror
</div>
<div class="input-group">
    <label for="hyperlink">Hyperlink:</label>
    <input id="hyperlink" type="text" name="hyperlink" value="{{ old('hyperlink', $cell->hyperlink ?? '') }}" required
           placeholder="https://example.com/"
           class="@error('hyperlink') invalid @enderror">
    @error('hyperlink')
        <div class="danger">{{ $message }}</div>
    @enderror
</div>
<div class="input-group">
    <label for="color">Color:</label>
    @foreach($colors as $color)
        <input type="radio"
               name="color_id"
               id="c{{ $color->id }}"
               value="{{ $color->id }}"
               @if (old('color_id', $cell->color_id ?? '') == $color->id) checked @endif>
        <label for="c{{ $color->id }}" style="background-color: {{ $color->hex_code }}" class="color"></label>
    @endforeach
    @error('color_id')
        <div class="danger">{{ $message }}</div>
    @enderror
</div>
