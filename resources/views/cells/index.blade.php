@extends('layouts.app')

@section('content')
    @for ($i = 0; $i <= $maxPosition; $i++)
        <div class="cell">
            @if(isset($cells[$i]))
               <a href="{{ $cells[$i]->hyperlink }}"
                  class="has-link link"
                  target="_blank"
                  style="background-color: {{$cells[$i]->color->hex_code}}"
               >
                   go
               </a>
                <a href="{{ route('cells.edit', ['cell' => $cells[$i]]) }}" class="edit">settings</a>
            @else
                <a href="{{ route('cells.create', ['position' => $i ]) }}" class="empty link">+</a>
            @endif
        </div>
    @endfor
@endsection
