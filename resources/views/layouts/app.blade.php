<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SHKOLO task</title>
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
</head>
<body>
    <header class="app-header">
    </header>
    <div class="app-wrapper">
        <div class="board-wrapper">
            <div id="scratch1" class="scratch"></div>
            <div id="scratch2" class="scratch"></div>
            <div id="scratch3" class="scratch"></div>
            <div id="scratch4" class="scratch"></div>
            @yield('content')
        </div>
    </div>
    <footer>

    </footer>
</body>
</html>
