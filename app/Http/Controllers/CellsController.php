<?php

namespace App\Http\Controllers;

use App\Cell;
use App\Color;


class CellsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        $cells = Cell::available();
        return view('cells.index', [
            'cells' => $cells,
            'maxPosition' => Cell::$availablePositionsEnd
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create()
    {
        $colors = Color::all();
        $request = $this->validatePosition();
        return view('cells.create', ['colors' => $colors, 'position' => $request['position']]);
    }

    /**
     * Store a newly created resource in storage.
     *
     */
    public function store()
    {
        Cell::create($this->validateRequest());
        return redirect(route('cells.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param Cell $cell
     */
    public function show(Cell $cell)
    {
        return redirect(route('cells.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     */
    public function edit(Cell $cell)
    {
        $colors = Color::all();
        return view('cells.edit', ['cell' => $cell, 'colors' => $colors]);
    }

    /**
     * Update the specified resource in storage.
     *
     */
    public function update(Cell $cell)
    {
        $cell->update($this->validateRequest());
        return redirect(route('cells.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     */
    public function destroy(Cell $cell)
    {
        $cell->delete();
        return redirect(route('cells.index'));
    }

    /**
     * Validate the request attributes.
     *
     * @return array
     */
    protected function validateRequest()
    {
        return request()->validate([
            'title' => 'required',
            'hyperlink' => 'required|url|active_url',
            'position' => 'sometimes|required|unique:cells,position|integer|between:'.Cell::$availablePositionsStart.','.Cell::$availablePositionsEnd,
            'color_id' => 'required|exists:colors,id'
        ]);
    }
    /**
     * Validate the position parameter.
     *
     * @return array
     */
    protected function validatePosition()
    {
        return request()->validate([
            'position' => 'required|unique:cells,position|integer|between:'.Cell::$availablePositionsStart.','.Cell::$availablePositionsEnd
        ]);
    }
}
