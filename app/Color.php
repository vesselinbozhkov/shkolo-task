<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    protected $fillable = ['name', 'hex_code'];

    public function cells() {
        return $this->hasMany(Cell::class);
    }
}
