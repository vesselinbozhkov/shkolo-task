<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cell extends Model
{
    protected $fillable = ['title', 'hyperlink', 'position', 'color_id'];

    public static $availablePositionsStart = 0;
    public static $availablePositionsEnd = 8;

    /**
     * The color of the cell.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function color() {
        return $this->belongsTo(Color::class);
    }

    /**
     * Gets the available cells.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public static function available() {
        return self::with('color')->whereBetween('position', [self::$availablePositionsStart, self::$availablePositionsEnd])->get()->keyBy('position');
    }
}
