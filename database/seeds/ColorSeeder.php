<?php

use Illuminate\Database\Seeder;

class ColorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('colors')->insert([
            'name' => 'розово',
            'hex_code' => '#ff66ff',
        ]);
        DB::table('colors')->insert([
            'name' => 'синьо',
            'hex_code' => '#0000ff',
        ]);
        DB::table('colors')->insert([
            'name' => 'жълто',
            'hex_code' => '#ffff66',
        ]);
        DB::table('colors')->insert([
            'name' => 'червено',
            'hex_code' => '#ff0000',
        ]);
        DB::table('colors')->insert([
            'name' => 'зелено',
            'hex_code' => '#00cc00',
        ]);
    }
}
